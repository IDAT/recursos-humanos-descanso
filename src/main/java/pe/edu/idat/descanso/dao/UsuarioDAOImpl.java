package pe.edu.idat.descanso.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.edu.idat.descanso.bean.UsuarioBean;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO{
	//TODO DECLARACIONES DAO
	static Log log = LogFactory.getLog(UsuarioDAOImpl.class.getName());	
	private JdbcTemplate jdbcTemplate;
    private int rows;
    private UsuarioBean usuarioBean = null;
    
	//TODO - JDBCTEMPLATE <-> recursos-humanos-descanso-servlet.xml
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
        
    @SuppressWarnings({ "unchecked", "rawtypes" })
	//TODO - LISTAR X CODIGO
	@Override
	public UsuarioBean selUsuarioByCodigo(String usuCodigo)  throws Exception{
        if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selUsuarioByCodigo");
        
        String SQL = "SELECT * FROM tbusuarios WHERE usu_codigo = ? ";        
		try {
			usuarioBean = (UsuarioBean) jdbcTemplate.queryForObject(SQL, new Object[] { usuCodigo }, new RowMapper(){
			            @Override
			            public UsuarioBean mapRow(ResultSet rs, int rowNum) throws SQLException
			            {
			            	UsuarioBean u = new UsuarioBean();
		                	u.setUsu_codigo(rs.getString("usu_codigo"));
		                	u.setUsu_nombre(rs.getString("usu_nombre"));
		                	u.setUsu_descri(rs.getString("usu_descri"));
		                	u.setUsu_passwd(rs.getString("usu_passwd"));
		                	u.setUsu_email(rs.getString("usu_email"));
		                	u.setUsu_imagen(rs.getString("usu_imagen"));
		                	u.setUsu_fecreg(rs.getTimestamp("usu_fecreg"));
		                	u.setUsu_estcod(rs.getInt("usu_estcod"));
			                return u;
			            }
			        });
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selUsuarioByCodigo");
			log.error(ex, ex);
			throw ex;	
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selUsuarioByCodigo");
		}
		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selectUsuarioByCodigo");
		
		return usuarioBean;
	}
	
	//TODO - LISTAR TODOS
	@Override
	public List<UsuarioBean> selUsuario() throws Exception{		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selectUsuario");
		
		List<UsuarioBean> lstUsuarios = null;
		String SQL ="SELECT * FROM tbusuarios";
		
		try {		
			lstUsuarios = jdbcTemplate.query(SQL, new ResultSetExtractor<List<UsuarioBean>>(){
	            @Override
	            public List<UsuarioBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<UsuarioBean> list = new ArrayList<UsuarioBean>();
	                while (rs.next())
	                {
	                	UsuarioBean u = new UsuarioBean();
	                	u.setUsu_codigo(rs.getString("usu_codigo"));
	                	u.setUsu_nombre(rs.getString("usu_nombre"));
	                	u.setUsu_descri(rs.getString("usu_descri"));
	                	u.setUsu_passwd(rs.getString("usu_passwd"));
	                	u.setUsu_email(rs.getString("usu_email"));
	                	u.setUsu_imagen(rs.getString("usu_imagen"));
	                	u.setUsu_fecreg(rs.getTimestamp("usu_fecreg"));
	                	u.setUsu_estcod(rs.getInt("usu_estcod"));		                	
	                    list.add(u);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selectUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selectUsuario");
		}
	    return lstUsuarios;
	}

	//TODO - INSERTAR
	@Override
	public boolean insUsuario(UsuarioBean usuario) throws Exception {		
        if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.insUsuario");		
		String SQL = "INSERT INTO tbusuarios VALUES(?,?,?,?,?,?,?,?)";	
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {usuario.getUsu_codigo(),
					 usuario.getUsu_nombre(),
					 usuario.getUsu_descri(),
					 usuario.getUsu_passwd(),
					 usuario.getUsu_email(),
					 usuario.getUsu_imagen(),
					 usuario.getUsu_fecreg(),
					 usuario.getUsu_estcod() });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.insUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.insUsuario");
		}
		return  rows ==1;
	}
	
	//TODO - ACTUALIZAR
	@Override
	public boolean updUsuario(UsuarioBean usuario) throws Exception {		
        if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.updUsuario");		
		String SQL = "UPDATE tbusuarios SET usu_descri = ?, usu_passwd = ?, usu_estcod = ?  WHERE usu_codigo = ?";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {usuario.getUsu_descri(), 
					 usuario.getUsu_passwd(), 
					 usuario.getUsu_estcod(), 
					 usuario.getUsu_codigo() });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.updUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.updUsuario");
		}
		return  rows ==1;
	}
	
	//TODO - ELIMINAR
	@Override
	public boolean delUsuario(String usuCodigo) throws Exception {		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.delUsuario");		
		String SQL = "DELETE FROM tbusuarios WHERE usu_codigo = ? ";		
		try {			
			rows = jdbcTemplate.update(SQL, new Object[] { usuCodigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.delUsuario");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.delUsuario");
		}	     
		return rows == 1;
	}
	
	
	//TODO - LOGIN
	@Override
	public UsuarioBean selUsuarioByCodigoAndPassword(String usuCodigo,String usuPassword) throws Exception {		
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");		
		
		try {					
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource()).withProcedureName("prcUsuarioLogin");
			 	MapSqlParameterSource params = new MapSqlParameterSource();
			 	params.addValue("in_nombre", usuCodigo);
	            params.addValue("in_passwd", usuPassword);
	            
			    Map<String, Object> out = jdbcCall.execute(params);

			    usuarioBean= new UsuarioBean();
			    usuarioBean.setUsu_codigo(usuCodigo);
			    usuarioBean.setUsu_passwd(usuPassword);
			      
			    usuarioBean.setUsu_descri((String) out.get("out_descri"));
			    usuarioBean.setUsu_email((String) out.get("out_email"));
			    usuarioBean.setUsu_estcod((Integer) out.get("out_estcod"));
			
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selUsuarioByCodigoAndPassword");
		}	     
		return usuarioBean;
	}
	
}
