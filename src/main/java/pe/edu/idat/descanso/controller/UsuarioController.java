package pe.edu.idat.descanso.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.sojo.interchange.json.JsonSerializer;
import pe.edu.idat.descanso.bean.UsuarioBean;
import pe.edu.idat.descanso.service.UsuarioService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Controller
@RequestMapping({"Usuario", "User" })
public class UsuarioController {	
	//TODO DECLARACIONES CONTROLLER
	static Log log = LogFactory.getLog(UsuarioController.class.getName());
	
	@Autowired		
	UsuarioService usuarioService;	
	boolean result;
	
	//TODO METODO GET MOSTRAR WEB -> MODALANDVIEW
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("UsuarioController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewUsuarios");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Usuario");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR USUARIOS 		
			List<UsuarioBean> lstUsuarios =usuarioService.listarUsuario();		
						
			modelo.addObject("listadoUsuarios", lstUsuarios);			
					
		} catch (Exception e) {
			log.error("UsuarioController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("UsuarioController.showView - Finalizando");
		return modelo;
	}
	
	//TODO METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  @ResponseBody Map<String, Object> getUsuarioAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.showView");				
			//TODO LISTAR USUARIOS 		
			List<UsuarioBean> lstUsuarios =usuarioService.listarUsuario();
			
			mapaResult.put("status", true);
			mapaResult.put("message", lstUsuarios.size() + " Registros Encontrados");
			mapaResult.put("data", lstUsuarios);
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.showView");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.showView");
		}		
		return mapaResult;
	}

	//TODO METODO GET MOSTRAR UN REGISTRO EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> getUsuario(@PathVariable String codigo) throws Exception  {	
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		 
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.getUsuario");	
			
			//TODO LISTAR X CODIGO		
			UsuarioBean usuario =usuarioService.listarUsuarioxCodigo(codigo);
			if(usuario != null) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registros Encontrados");
				mapaResult.put("data", usuario);
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registros Encontrados");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.getUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.getUsuario");
		}
		
		return mapaResult;
	}
	
	//TODO METODO POST INSERTAR UN REGISTRO  <- JSON
	@RequestMapping(value="/Rest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> postUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.postUsuario");			
			//LECTURA DEL REQUEST BODY JSON
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        //TODO SERIALIZAMOS EL JSON -> CLASS 
			UsuarioBean usuarioNuevo = (UsuarioBean) new JsonSerializer().deserialize(jsonEnviado.toString(), UsuarioBean.class);			
			//----------------------------------------------
			//TODO SERVICE INSERTAR USUARIO
			result = usuarioService.insertarUsuario(usuarioNuevo);
			//----------------------------------------------
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Guardado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Guardado");
			}			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.postUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.postUsuario");			
		}			
		return mapaResult;
	}
	
	//TODO METODO PUT ACTUALIZAR UN REGISTRO  <- JSON
		@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> putUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.putUsuario");			
			//LECTURA DEL REQUEST BODY
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
			//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
			UsuarioBean usuarioNuevo = (UsuarioBean) new JsonSerializer().deserialize(jsonEnviado.toString(), UsuarioBean.class);			
			
			//TODO ACTUALIZAR USUARIO
			result = usuarioService.actualizarUsuario(usuarioNuevo);
			
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Actualizado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Actualizado");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.putUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.putUsuario");
			
		}			
		return mapaResult;
	}
	
	//TODO METODO DELETE ELIMINAR UN REGISTRO  <- PARAMETER
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> delUsuario(@PathVariable String codigo) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.delUsuario");			
			//TODO ELIMINAR USUARIO
			result = usuarioService.eliminarUsuarioxCodigo(codigo);
			
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registros Eliminado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registros Eliminado");
			}			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.delUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.delUsuario");
		}		
		return mapaResult;
	}
	
	//TODO METODO POST LOGIN USUARIO EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> LogUsuario(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		 
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.getUsuario");
			
			//TODO LECTURA DEL REQUEST BODY JSON <- {"USU":"ATORRES","PWD":"456"}
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        //TODO SERIALIZAMOS EL JSON -> MAP
			@SuppressWarnings("unchecked")
			Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado.toString(), Map.class);
			//----------------------------------------------
			//TODO SERVICIO LISTAR USUARIO X CODIGO & PASSWORD		
			UsuarioBean usuarioBean =usuarioService.listarUsuarioxCodigoAndPassword(dataEnvio.get("USU").toString(), dataEnvio.get("PWD").toString());
			//----------------------------------------------
			if(usuarioBean != null) {
				mapaResult.put("status", true);
				mapaResult.put("message", "BIENVENIDO " + usuarioBean.getUsu_descri() );
				mapaResult.put("data", usuarioBean);
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "Usuario o Password errados.");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.getUsuario");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.getUsuario");
		}
		
		return mapaResult;
	}
		
}
