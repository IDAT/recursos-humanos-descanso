package pe.edu.idat.descanso.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.descanso.bean.UsuarioBean;
import pe.edu.idat.descanso.dao.UsuarioDAOImpl;

@Service("UsuarioService")
public class UsuarioServiceImp implements UsuarioService {
	//TODO DECLARACIONES SERVICE
	private static final Log log = LogFactory.getLog(UsuarioServiceImp.class);	
	@Autowired
	private UsuarioDAOImpl usuarioDAO;
	private boolean result;
	UsuarioBean usuarioBean;	
	
	//TODO METODO LISTAR TODOS
	@Override
	public List<UsuarioBean> listarUsuario() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioServiceImp.listarUsuario");
		List<UsuarioBean> lst = new ArrayList<UsuarioBean>();	
		try {
			lst = usuarioDAO.selUsuario();
			//TODO POLITICA DE NEGOCIO
			for (UsuarioBean item : lst) {
				int estado = item.getUsu_estcod();				
				item.setUsu_estdes((estado == 0 ? "INACTIVO" : "ACTIVO"));			
			}			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.listarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.listarUsuario");
		} 	
		return lst;
	}
	
	//TODO METODO LISTAR X CODIGO
	@Override
	public UsuarioBean listarUsuarioxCodigo(String usuCodigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioServiceImp.listarUsuarioxCodigo");		
		try {
			usuarioBean = usuarioDAO.selUsuarioByCodigo(usuCodigo);			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.listarUsuarioxCodigo");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.listarUsuarioxCodigo");
		} 	
		return usuarioBean;
	}
	
	//TODO METODO INSERTAR
	@Override
	public boolean insertarUsuario(UsuarioBean u) throws Exception {
		try {
			result = usuarioDAO.insUsuario(u);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.insertarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.insertarUsuario");
		} 	
		return result;
	}
	
	//TODO METODO ACTUALIZAR
	@Override
	public boolean actualizarUsuario(UsuarioBean u) throws Exception {
		try {
			result = usuarioDAO.updUsuario(u);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.actualizarUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.actualizarUsuario");
		} 	
		return result;
	}
	
	//TODO METODO ELIMINAR X CODIGO
	@Override
	public boolean eliminarUsuarioxCodigo(String usuCodigo) throws Exception {
		try {
			result = usuarioDAO.delUsuario(usuCodigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.eliminarUsuarioxCodigo");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.eliminarUsuarioxCodigo");
		} 	
		return result;
	}
	
	//TODO METOFO LOGIN
	@Override
	public UsuarioBean listarUsuarioxCodigoAndPassword(String usuCodigo, String usuPassword) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioServiceImp.listarUsuarioxCodigoxPassword");
		try {
			usuarioBean = usuarioDAO.selUsuarioByCodigoAndPassword(usuCodigo, usuPassword);			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - UsuarioServiceImp.listarUsuarioxCodigoxPassword");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioServiceImp.listarUsuarioxCodigoxPassword");
		} 	
		return usuarioBean;
	}
}