package pe.edu.idat.descanso.model;

import java.io.Serializable;

public abstract class EmpresaModel implements Serializable{
	private static final long serialVersionUID = 1L;
	String emp_codigo;
	String emp_descri;
	int emp_estcod;
	
	public String getEmp_codigo() {
		return emp_codigo;
	}
	public void setEmp_codigo(String emp_codigo) {
		this.emp_codigo = emp_codigo;
	}
	public String getEmp_descri() {
		return emp_descri;
	}
	public void setEmp_descri(String emp_descri) {
		this.emp_descri = emp_descri;
	}
	public int getEmp_estcod() {
		return emp_estcod;
	}
	public void setEmp_estcod(int emp_estcod) {
		this.emp_estcod = emp_estcod;
	}
	
	
}