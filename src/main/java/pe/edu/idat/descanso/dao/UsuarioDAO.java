package pe.edu.idat.descanso.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.edu.idat.descanso.bean.UsuarioBean;
@Repository
public interface UsuarioDAO {
	 
    public UsuarioBean selUsuarioByCodigo(String usuCodigo) throws Exception;
     
    public List<UsuarioBean> selUsuario() throws Exception;
    
    public boolean insUsuario(UsuarioBean usuario)throws Exception;
    
    public boolean updUsuario(UsuarioBean usuario)throws Exception;
    
    public boolean delUsuario(String usuCodigo)throws Exception; 
    
    public UsuarioBean selUsuarioByCodigoAndPassword(String usuCodigo,String usuPassword) throws Exception;
}
