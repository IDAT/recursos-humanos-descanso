package pe.edu.idat.descanso.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import pe.edu.idat.descanso.bean.EmpresaBean;


public class EmpresaDAOImpl implements EmpresaDAO{
	//TODO DECLARACIONES DAO
	static Log log = LogFactory.getLog(EmpresaDAOImpl.class.getName());	
	private JdbcTemplate jdbcTemplate;

	//TODO - JDBCTEMPLATE <-> recursos-humanos-descanso-servlet.xml
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	@Override
	public List<EmpresaBean> selEmpresa() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - UsuarioDAOImpl.selectUsuario");
		
		List<EmpresaBean> lstEmpresas = null;
		String SQL ="SELECT * FROM tbempresas";
		
		try {		
			lstEmpresas = jdbcTemplate.query(SQL, new ResultSetExtractor<List<EmpresaBean>>(){
	            @Override
	            public List<EmpresaBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<EmpresaBean> list = new ArrayList<EmpresaBean>();
	                while (rs.next())
	                {
	                	EmpresaBean e = new EmpresaBean();
	                	e.setEmp_codigo(rs.getString("emp_codigo"));	                	
	                	e.setEmp_descri(rs.getString("emp_descri"));	                	
	                	e.setEmp_estcod(rs.getInt("emp_estcod"));		                	
	                    list.add(e);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioDAOImpl.selectUsuario");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - UsuarioDAOImpl.selectUsuario");
		}
	    return lstEmpresas;
	}

}
