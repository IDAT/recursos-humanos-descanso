package pe.edu.idat.descanso.service;

import java.util.List;

import pe.edu.idat.descanso.bean.EmpresaBean;

public interface EmpresaService {
	
	public List<EmpresaBean> listarEmpresas() throws Exception;	

}
