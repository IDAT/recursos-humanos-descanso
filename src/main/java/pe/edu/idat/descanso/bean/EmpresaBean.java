package pe.edu.idat.descanso.bean;

import pe.edu.idat.descanso.model.EmpresaModel;

public class EmpresaBean extends EmpresaModel{
	private static final long serialVersionUID = 1L;
	String emp_estdes;
	public String getEmp_estdes() {
		return emp_estdes;
	}
	public void setEmp_estdes(String emp_estdes) {
		this.emp_estdes = emp_estdes;
	}

}