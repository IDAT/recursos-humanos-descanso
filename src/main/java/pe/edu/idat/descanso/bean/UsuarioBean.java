package pe.edu.idat.descanso.bean;

import pe.edu.idat.descanso.model.UsuarioModel;

public class UsuarioBean extends UsuarioModel {
	private static final long serialVersionUID = 1L;
	String usu_estdes;

	public String getUsu_estdes() {
		return usu_estdes;
	}

	public void setUsu_estdes(String usu_estdes) {
		this.usu_estdes = usu_estdes;
	}
		
}