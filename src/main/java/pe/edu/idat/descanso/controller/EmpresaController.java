package pe.edu.idat.descanso.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.edu.idat.descanso.bean.EmpresaBean;
import pe.edu.idat.descanso.bean.UsuarioBean;
import pe.edu.idat.descanso.service.EmpresaService;


@Controller
@RequestMapping("Empresa")
public class EmpresaController {
	//TODO DECLARACIONES CONTROLLER
	static Log log = LogFactory.getLog(EmpresaController.class.getName());

	@Autowired		
	EmpresaService empresaService;
	
	//TODO METODO GET MOSTRAR TODOS LOS REGISTROS EN JSON -> RESPONSEBODY	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  @ResponseBody Map<String, Object> getUsuarioAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - UsuarioController.showView");				
			//TODO LISTAR USUARIOS 		
			List<EmpresaBean> lstEmpresas =empresaService.listarEmpresas();
			
			mapaResult.put("status", true);
			mapaResult.put("message", lstEmpresas.size() + " Registros Encontrados");
			mapaResult.put("data", lstEmpresas);
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - UsuarioController.showView");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - UsuarioController.showView");
		}		
		return mapaResult;
	}	
}
