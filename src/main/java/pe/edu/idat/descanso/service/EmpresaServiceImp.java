package pe.edu.idat.descanso.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.idat.descanso.bean.EmpresaBean;
import pe.edu.idat.descanso.dao.EmpresaDAOImpl;

@Service("EmpresaService")
public class EmpresaServiceImp implements EmpresaService {
	//TODO DECLARACIONES SERVICE
	private static final Log log = LogFactory.getLog(UsuarioServiceImp.class);	
	@Autowired
	private EmpresaDAOImpl empresaDAO;

	
	@Override
	public List<EmpresaBean> listarEmpresas() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - EmpresaServiceImpl.listarEmpresas");
		List<EmpresaBean> lst = new ArrayList<EmpresaBean>();	
		try {
			lst = empresaDAO.selEmpresa();
			//TODO POLITICA DE NEGOCIO
			for (EmpresaBean item : lst) {
				int estado = item.getEmp_estcod();				
				item.setEmp_estdes((estado == 0 ? "INACTIVO" : "ACTIVO"));			
			}			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - EmpresaServiceImpl.listarEmpresas");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - EmpresaServiceImpl.listarEmpresas");
		} 	
		return lst;
	}

}