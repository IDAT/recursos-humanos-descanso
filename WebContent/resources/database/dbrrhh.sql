-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2018 a las 04:38:05
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbrrhh`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioLogin` (IN `in_nombre` VARCHAR(20), IN `in_passwd` VARCHAR(20), OUT `out_descri` VARCHAR(20), OUT `out_email` VARCHAR(20), OUT `out_estcod` INTEGER)  BEGIN
   SELECT usu_descri, usu_email, usu_estcod
   INTO out_descri, out_email,out_estcod
   FROM tbusuarios where usu_nombre = in_nombre and usu_passwd = in_passwd;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbusuarios`
--

CREATE TABLE `tbusuarios` (
  `usu_codigo` char(3) NOT NULL,
  `usu_nombre` varchar(50) NOT NULL,
  `usu_descri` varchar(50) NOT NULL,
  `usu_passwd` varchar(50) NOT NULL,
  `usu_email` varchar(50) NOT NULL,
  `usu_imagen` varchar(50) NOT NULL,
  `usu_fecreg` date NOT NULL,
  `usu_estcod` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbusuarios`
--

INSERT INTO `tbusuarios` (`usu_codigo`, `usu_nombre`, `usu_descri`, `usu_passwd`, `usu_email`, `usu_imagen`, `usu_fecreg`, `usu_estcod`) VALUES
('001', 'ATORRES', 'ALEX TORRES', '456', 'superahacker@gmail.com', '', '2018-01-21', 1),
('002', 'CTORRES', 'CIELO TORRES', '789', '', '', '2018-01-21', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbusuarios`
--
ALTER TABLE `tbusuarios`
  ADD PRIMARY KEY (`usu_codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
