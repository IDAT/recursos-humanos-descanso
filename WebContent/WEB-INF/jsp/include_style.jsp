<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>INSPINIA | Login 2</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- let's add tag srping:url -->	
	<spring:url value="/resources/twitter-bootstrap-v3.3.7/css/bootstrap.min.css" var="bootstrapCSS" />
    <!--  <link href="${bootstrapCSS}" rel="stylesheet" /> -->
    
    <link href="/a/twitter-bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/a/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        
    <!-- Toastr style -->
    <!--  <link href="<spring:url value="/resources/toastr.js/css/toastr.min.css" />" rel="stylesheet" type="text/css"/>  -->
    <link href="/a/toastr.js/css/toastr.min.css" rel="stylesheet" type="text/css"/>
    <!-- FooTable -->
    <link href="/a/jquery-footable/css/footable.core.min.css" rel="stylesheet" type="text/css"/>
    
    <!-- Gritter -->
    <link href="/a/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    
    <!-- DatetimePicker-->
    <link href="/a/jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    
    <link href="/a/animate.css/animate.min.css" rel="stylesheet" type="text/css"/>
    
    <link href="/a/css/style.css" rel="stylesheet" type="text/css"/>
</head>