package pe.edu.idat.descanso.dao;

import java.util.List;

import pe.edu.idat.descanso.bean.EmpresaBean;

public interface EmpresaDAO {
	
    public List<EmpresaBean> selEmpresa() throws Exception;

}
